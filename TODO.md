- Accept custom display options
- Distinction between viewport size and world size
- randBetween test
- map generator test
- update mock 'anys' to Jest function
- GameEngine tests
- real tests for player action
- bin the player controller in favour of more featured Guides?
- Test the Draw function on GameWorld
- Tests for FOV
- Mouse control
- Improve the path generator, test it
- Standardise the interface when dealing with entities - refernece by entitiy or co-ordinate?
- Move more control into Guide. Maybe add a 'deploy' method which sets it up on teh game world separately from constructing it?
- Infinite loop issue
    - why are lost guides getting activated? Probably a missing checkTeamVisibility
- skip turn
