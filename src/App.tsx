import React from 'react';
import './App.css';
import SpiritGuides from './SpiritGuides';

function App() {
  return (
    <div className="App">
        <SpiritGuides></SpiritGuides>
    </div>
  );
}

export default App;
