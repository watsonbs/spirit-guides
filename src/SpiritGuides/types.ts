export type Coordinate = {
    x: number
    y: number
};

export enum Facing {
    NORTH = 0,
    NORTH_EAST,
    EAST,
    SOUTH_EAST,
    SOUTH,
    SOUTH_WEST,
    WEST,
    NORTH_WEST
}

export interface Actor {
    act: ()=> Promise<void>
}
export interface Entity extends Actor {
    isPassable(): boolean
    isSeeThrough(): boolean
    getAvatar(): string
    setLocation(location: Coordinate): void
    getLocation(): Coordinate
    isPlayer(): boolean
    lose(): void
}
