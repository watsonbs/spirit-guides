import Team from './Team';
import randBetween from '../utils/randBetween';

jest.mock('../utils/randBetween', ()=> jest.fn().mockReturnValue(0));

describe('Team', () => {
    test('creates a team of specified size with unique ids and avatars', () => {
        const team = new Team(3);
        expect(team.getMembers().length).toEqual(3);
        expect(team.getMembers()[0].getId()).toBe('1');
        expect(team.getMembers()[0].getAvatar()).toBe('👧🏻');
        expect(team.getMembers()[1].getId()).toBe('2');
        expect(team.getMembers()[1].getAvatar()).toBe('👧🏼');
        expect(team.getMembers()[2].getId()).toBe('3');
        expect(team.getMembers()[2].getAvatar()).toBe('👧🏽');
        const smallTeam = new Team(1);
        expect(smallTeam.getMembers().length).toEqual(1);
        expect(smallTeam.getMembers()[0].getId()).toBe('1');
        expect(team.getMembers()[0].getAvatar()).toBe('👧🏻');
        expect(randBetween).toHaveBeenCalledTimes(12);
    });
    test('addWatcher adds a watcher to be called if the team is updated', () => {
        const team = new Team(3);
        const watcher = jest.fn();
        team.addWatcher(watcher);
        team.hasUpdated();
        expect(watcher).toHaveBeenCalled();

    });
    test('when a team member is updated it triggers the watchers', () => {
        const team = new Team(1);
        const watcher = jest.fn();
        team.addWatcher(watcher);
        team.getMembers()[0].activate();
        expect(watcher).toHaveBeenCalled();
    });
});
