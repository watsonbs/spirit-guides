import randBetween from '../utils/randBetween';
import Guide from './Guide/Guide';

export default class Team {
    private idIterator = 0;
    private availableAvatars = [
        "👧🏻",
        "👧🏼",
        "👧🏽",
        "👧🏾",
        "👧🏿"
    ]
    private members: Guide[]
    private watchers: Function[]
    constructor(guideCount: number) {
        this.watchers = []; 
        this.members = [];
        for(let i=0; i<guideCount; i++) {
            this.createTeamMember();
        }
    }
    createTeamMember() { 
        this.idIterator += 1;
        const avatar = this.availableAvatars.splice(randBetween(0, this.availableAvatars.length-1),1)[0];
        this.members.push(new Guide(`${this.idIterator}`, avatar, () => this.hasUpdated()));
    }
    getMembers() { return this.members.filter(member => !member.isLost()) }
    addWatcher(watcherFunc: Function) { this.watchers.push(watcherFunc)}
    hasUpdated() { this.watchers.forEach(watcher => watcher())}
}
