import generateName from "./generateName";
import Guide from "./Guide";

jest.mock('./generateName');
const mockGenerateName = generateName as jest.MockedFunction<()=> string>

describe('Guide', () => {
    test('new guides have a name', () => {
        mockGenerateName.mockReturnValue('firstname surname');
        const guide = new Guide('1', 'A', jest.fn());
        expect(guide.getName()).toBe('firstname surname');
    });
    test('new guides accept an id and avatar', () => {
        const aGuide = new Guide('1', 'A', jest.fn());
        const bGuide = new Guide('2', 'B', jest.fn());
        expect(aGuide.getId()).toBe('1');
        expect(aGuide.getAvatar()).toBe('A');
        expect(bGuide.getId()).toBe('2');
        expect(bGuide.getAvatar()).toBe('B');
    });
    test('new guides have 3 action points', () => {
        const guide = new Guide('1', 'A', jest.fn());
        expect(guide.getMaximumActionPoints()).toBe(3);
        expect(guide.getAvailableActionPoints()).toBe(3);
    });
    test('isPlayer returns true', () => {
        const guide = new Guide('1', 'A');
        expect(guide.isPlayer()).toBe(true);
    })

    test('calling act on a Guide triggers its action callback', () => {
        const mockAction = jest.fn();
        const guide = new Guide('1', 'A', jest.fn());
        guide.setAction(mockAction);
        guide.act();
        expect(mockAction).toHaveBeenCalled();
    });

    test('subtractActionPoints removes that number of action points', () => {
        const guide = new Guide('1', 'A', jest.fn());
        expect(guide.getAvailableActionPoints()).toBe(3);
        guide.subtractActionPoints(1);
        expect(guide.getAvailableActionPoints()).toBe(2);
        guide.subtractActionPoints(2);
        expect(guide.getAvailableActionPoints()).toBe(0);
    });

    test('subtractActionPoints throws an error if there are not enough action points', () => {
        const guide = new Guide('1', 'A', jest.fn());
        try {
            guide.subtractActionPoints(10);
            expect('error not thrown').toBe(false);
        } catch (error) {
            expect(error).toEqual(new Error('Not enough action points'));
        }
    });

    test('activate and deactivate sets the guide active state', () => {
        const render = jest.fn();
        const guide = new Guide('1', 'A', render);
        expect(guide.isActive()).toBe(false);
        guide.activate();
        expect(guide.isActive()).toBe(true);
        expect(render).toHaveBeenCalled();
        render.mockClear();
        guide.deactivate();
        expect(guide.isActive()).toBe(false);
        expect(render).toHaveBeenCalled();
    });

    test('getVisionRange returns 10', () => {
        const guide = new Guide('1');
        expect(guide.getVisionRange()).toBe(10);
    });

    test('lose marks the guide as lost and ends their active turn', () => {
        const renderCallback = jest.fn();
        const guide = new Guide('1', 'A', renderCallback);
        guide.activate();
        guide.lose();
        expect(guide.isLost()).toBe(true);
        expect(guide.isActive()).toBe(false);
        expect(guide.getAvailableActionPoints()).toBe(0);
        expect(renderCallback).toHaveBeenCalled();
    });

    test('find unmarks the guide as lost', () => {
        const guide = new Guide('1');
        guide.lose();
        guide.find();
        expect(guide.isLost()).toBe(false);
    })
});
