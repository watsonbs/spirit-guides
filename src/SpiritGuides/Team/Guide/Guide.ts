import {Coordinate, Entity } from '../../types';
import generateName from './generateName';

export default class Guide implements Entity {
    private id: string
    private name: string
    private actionPoints: number
    private availableActionPoints: number
    private avatar: string
    private location: Coordinate
    private active: boolean
    private render: Function
    private lost: boolean
    act: () => Promise<void>
    constructor(id: string, avatar: string = 'X', renderCallback: Function = () => {}) {
        this.name = generateName();
        this.actionPoints = 3;
        this.availableActionPoints = this.actionPoints;
        this.id = id;
        this.avatar = avatar;
        this.active = false;
        this.act = async () => {};
        this.render = renderCallback;
        this.location = {x: -1, y: -1};
        this.lost = false;
    }
    setAction(action: () => Promise<void>) { this.act = action; }
    setLocation(location: Coordinate) { this.location = location; }
    getLocation(){ return  this.location; }
    getId() { return this.id}
    getAvatar() { return this.avatar}
    getName() { return this.name}
    getMaximumActionPoints() { return this.actionPoints}
    getAvailableActionPoints() { return this.availableActionPoints}
    subtractActionPoints(amount: number) { 
        const newTotal = this.availableActionPoints - amount;
        if(newTotal < 0) throw Error("Not enough action points");
        this.availableActionPoints = newTotal;
    }
    find() { 
        this.lost = false 
        this.render();
    }
    lose() { 
        this.lost = true 
        this.availableActionPoints = 0;
        this.deactivate();
    }
    isLost() { return this.lost }
    isPassable() { return false}
    isSeeThrough() { return true }
    isPlayer() { return true }
    isActive() { return this.active }
    activate() { 
        this.active = true 
        this.availableActionPoints = this.actionPoints;
        this.render();
    }
    deactivate() { 
        this.active = false 
        this.render();
    }
    getVisionRange() { return 10}
    
}
