import randBetween from "../../utils/randBetween";

export default () => {
    const firstNames = [
        'Alice',
        'Jack',
        'Alyssa',
        'Lara',
        'George',
        'Alex',
        'Sam',
        'Max',
        'Maisie',
        'Harper',
        'Parker',
        'Stevie',
    ];

    const secondNames = [
        'Savage',
        'Grey',
        'Heartnell',
        'Spendlove',
        'Hardwall',
        'Wallace',
        'Macleod',
        'Winters',
        'Summers',
        'Hound'
    ]

    const firstName = firstNames[randBetween(0, firstNames.length-1)];
    const secondName = secondNames[randBetween(0, secondNames.length-1)];

    return `${firstName} ${secondName}`
}
