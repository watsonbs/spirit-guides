import initialiseGameDisplay from './initialiseGameDisplay';
import ROT from 'rot-js';
import { render } from '@testing-library/react';

jest.mock('rot-js', () => {
    return {
        Display: jest.fn()
    }
});

const rotDisplayMock = ROT.Display as any



describe('GameDisplay', () => {
    test('initialiseGameDisplay initialises a new Display and returns it', () => {
        const myDisplayOptions = { width: 1, height: 1};
        const myDisplayContainer = { isMyDisplayContainer: true};
        const instantiatedDisplayMock = { getContainer: jest.fn().mockReturnValue(myDisplayContainer)};
        render(<div id="canvas"></div>);
        
        rotDisplayMock.mockImplementation(() => {
            return instantiatedDisplayMock
        });
        const canvas = document.getElementById('canvas') || {} as any;
        canvas.appendChild = jest.fn();

        const gameDisplay = initialiseGameDisplay(myDisplayOptions);
        expect(ROT.Display).toHaveBeenCalledWith(myDisplayOptions);
        expect(canvas.appendChild).toHaveBeenCalledWith(myDisplayContainer);
        expect(gameDisplay).toBe(instantiatedDisplayMock)
    });

    it('returns an error if no canvas container element is available', () => {
        render(<div></div>);
        try {
            const gameDisplay = initialiseGameDisplay();
            expect('error was thrown').toBe(true);
        } catch(error) {
            expect(error.message).toBe('No Canvas Container')
        }
    });

    it('removes existing childNodes if there are any in the canvas element', () => {
        render(<div id="canvas"><span></span></div>);
        const myDisplayContainer = { isMyDisplayContainer: true};
        rotDisplayMock.mockImplementation(() => {
            return { getContainer: jest.fn().mockReturnValue(myDisplayContainer)}
        });
        const canvas = document.getElementById('canvas') || {} as any;
        const child = canvas.childNodes[0];
        canvas.appendChild = jest.fn();
        canvas.removeChild = jest.fn();

        const gameDisplay = initialiseGameDisplay();

        expect(canvas.removeChild).toHaveBeenCalledWith(child);

    })
});
