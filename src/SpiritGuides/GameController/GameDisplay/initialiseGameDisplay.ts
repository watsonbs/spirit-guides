import { Display } from 'rot-js';
import { DisplayOptions } from 'rot-js/lib/display/types';

const initialiseGameDisplay = (displayOptions: Partial<DisplayOptions> = {}):Display => {
    const canvasContainer = document.getElementById("canvas") as HTMLCanvasElement;
    if(!canvasContainer) {
        throw Error('No Canvas Container');
    }
    const existingContent = canvasContainer.childNodes[0];
    if(existingContent) {
        canvasContainer.removeChild(existingContent);
    }

    const gameDisplay = new Display(displayOptions);
    canvasContainer.appendChild(gameDisplay.getContainer() as Node);
    return gameDisplay;
};

export default initialiseGameDisplay
