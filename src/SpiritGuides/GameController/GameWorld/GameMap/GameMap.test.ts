import ROT from 'rot-js';
import GameMap from './GameMap';
import GameCell from './GameCell/GameCell';
jest.mock('rot-js', () => {
    return {
        Map: {
            Cellular: jest.fn()
        }
    }
});

const rotMapCellularMock = ROT.Map.Cellular as any

describe('GameMap', () => {
    const instantiatedMapCellularMock = { 
        randomize: jest.fn(),
        create: jest.fn()
    };
    beforeEach(() => {
        rotMapCellularMock.mockImplementation(() => {
            return instantiatedMapCellularMock
        });
    });

    describe('instantiation', () => {
        test('the first column is unpassable 🔥', () => {
            const gameMap = new GameMap(15, 15);
            const firstColumn = gameMap.getMap()[0];
            firstColumn.forEach(cell => {
                expect(cell.isPassable()).toBe(false);
                expect(cell.getContent()).toBe("🔥");
            });
        });
        test('the first and last rows are unpassable 🌲', () => {
            const gameMap = new GameMap(15, 15);
            const map = gameMap.getMap();
            for(let i = 1; i < gameMap.getWidth(); i++) {
                const topRow = map[i][0];
                const bottomRow = map[i][gameMap.getHeight()-1];
                expect(topRow.getContent()).toBe('🌲');
                expect(topRow.isPassable()).toBe(false);
                expect(bottomRow.getContent()).toBe('🌲');
                expect(bottomRow.isPassable()).toBe(false);
            }
        });
        test('populates the map with a cellar set of 🌳', () => {
            const width = 15;
            const height = 15;
            const gameMap = new GameMap(width, height);
            expect(instantiatedMapCellularMock.randomize).toHaveBeenCalledWith(0.5);
            expect(rotMapCellularMock).toHaveBeenCalledWith(width-1, height-2);
            //TODO - test the create function call
        });
    });

    test('getCell returns the content of the cell at the given Coordinate', () => {
        const gameMap = new GameMap(15,15);
        expect(gameMap.getCell({x: 0, y: 0})).toEqual(new GameCell('🔥'))
        expect(gameMap.getCell({x: 1, y: 0})).toEqual(new GameCell('🌲'))
    })

    test('clearCell makes the content of the given cell into an empty cell', () => {
        const gameMap = new GameMap(15,15);
        gameMap.clearCell({x: 1, y:0});
        expect(gameMap.getCell({x: 1, y: 0})).toEqual(new GameCell('.', {isPassable: true, colour: "lightgrey"}))
    });

    test('exploreLeft adds a column to the end', () => {
        const gameMap = new GameMap(10,10);
        gameMap.exploreLeft();
        expect(gameMap.getWidth()).toBe(11);
        expect(gameMap.getCell({x:10,y:9})).toBeTruthy();
    });
    test('destroyColumn removes a column', () => {
        const gameMap = new GameMap(10,10);
        gameMap.destroyColumn(1);
        expect(gameMap.getWidth()).toBe(9);
        expect(gameMap.getCell({x:9,y:1})).toBeFalsy();
    });
});
