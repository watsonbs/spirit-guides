import { Map } from "rot-js";
import {Coordinate} from "../../../types";
import randBetween from "../../../utils/randBetween";
import GameCell from "./GameCell/GameCell";

const emptyCellOptions = {
  content: '.',
  isPassable: true,
  colour: 'lightgrey'
}

export default class GameMap {
    private map: GameCell[][]
    private width: number
    private height: number
    constructor(width: number, height: number){
        this.width = width;
        this.height = height;
        const map: GameCell[][] = [];
        map[0] = [];
        for(let j = 0; j < height; j++) {
            map[0][j] = new GameCell("🔥");
        }
        for (let i = 1; i < width; i++) {
            map[i] = [];
            map[i][0] = new GameCell("🌲");
            map[i][height-1] = new GameCell("🌲");
        }
        const digger = new Map.Cellular(
            width - 1,
            height - 2
        );
    
        digger.randomize(0.5);
        digger.create((x, y, value) => {
          if (value) {
            map[x + 1][y + 1] = new GameCell('🌳');
          } else {
            map[x + 1][y + 1] = new GameCell(emptyCellOptions.content, emptyCellOptions);
          }
        });
        this.map = map;
        this.createMapPath({x:3,y:1}, {x:width-1, y: height-2});
    }
    private createMapPath(start: Coordinate, end: Coordinate) {
      this.clearCell(start);
      const location = start;
      while(location.x < end.x || location.y < end.y) {
        const needsX = location.x < end.x;
        const needsY = location.y < end.y;
        const moveDirection = needsX ?
          needsY ? 
            randBetween(0,1) ? 
              'x' 
            : 'y' 
          : 'x' 
        : 'y';

        location[moveDirection] = location[moveDirection]+1;
        this.clearCell(location);
      }
    }
    getCell(coordinate: Coordinate): GameCell|null { 
      const col = this.map[coordinate.x];
      if(!col) return null;
      return col[coordinate.y];
    }
    clearCell(coordinate: Coordinate) { 
      const cell = this.getCell(coordinate);
      if(cell) {
        cell.changeCell(emptyCellOptions);
      }
    }
    getMap() { return this.map }
    getWidth() { return this.width}
    getHeight() { return this.height}
    exploreLeft() {
      const newColumn = [new GameCell('🌲')];
      for(let i = 1; i < this.height-1; i++) {
        newColumn[i] = randBetween(0,2) > 1 ? new GameCell('🌳') : new GameCell(emptyCellOptions.content, emptyCellOptions);
      }
      newColumn[this.height-1] = new GameCell('🌲');
      this.map[this.map.length] = newColumn;
      this.width = this.width+1;
    }
    destroyColumn(column: number) {
      this.map.splice(column,1);
      this.width = this.width -1;
    }
};
