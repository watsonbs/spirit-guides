import Guide from '../../../../Team/Guide/Guide';
import GameCell from './GameCell';

describe('GameCell', () => {
    test('sets content and default options if no options are provided', () => {
        const cell = new GameCell('A');
        expect(cell.getContent()).toBe('A');
        expect(cell.isPassable()).toBe(false);
        expect(cell.getColour()).toBe('dimGrey');
        expect(cell.getBgColour()).toBe('rosybrown');
    });
    test('sets isPassable to true if it is specified in the options', () => {
        const cell = new GameCell('A', {isPassable: true});
        expect(cell.isPassable()).toBe(true);
    });
    test('sets colour if it is specified in the options', () => {
        const cell = new GameCell('A', {colour: 'pink'});
        expect(cell.getColour()).toBe('pink');
    });
    test('sets bgColour if it is specified in the options', () => {
        const cell = new GameCell('A', {bgColour: 'pink'});
        expect(cell.getBgColour()).toBe('pink');
    });
    test('changeCell updates the content of the cell', () => {
        const cell = new GameCell('A', {isPassable: true, colour: 'grey', bgColour: 'pink'});
        cell.changeCell({ content: 'B', isPassable: false, colour: 'black', bgColour: 'yellow'});
        expect(cell.getBgColour()).toBe('yellow');
        expect(cell.getColour()).toBe('black');
        expect(cell.getContent()).toBe('B');
        expect(cell.isPassable()).toBe(false);
    });
    describe('visibility', () => {
        test('addViewer adds to the cell visibility if they are not already a viewer', ()=> {
            const cell = new GameCell('A', {isPassable: true, colour: 'grey', bgColour: 'pink'});
            const guide = new Guide('1');
            expect(cell.getVisibility()).toBe(0);
            cell.addViewer(guide);
            expect(cell.getVisibility()).toBe(1);
            cell.addViewer(guide);
            expect(cell.getVisibility()).toBe(1);
            const newGuide = new Guide('2');
            cell.addViewer(newGuide);
            expect(cell.getVisibility()).toBe(2);
        });
        test('removeViewer removes a viewer if it is present', ()=> {
            const cell = new GameCell('A', {isPassable: true, colour: 'grey', bgColour: 'pink'});
            const guide = new Guide('1');
            cell.removeViewer(guide);
            expect(cell.getVisibility()).toBe(0);
            cell.addViewer(guide);
            expect(cell.getVisibility()).toBe(1);
            cell.removeViewer(guide);
            expect(cell.getVisibility()).toBe(0);
        });
    });
});
