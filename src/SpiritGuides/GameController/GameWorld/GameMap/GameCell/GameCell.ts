import { Entity } from "../../../../types"

export type GameCellOptions = {
    isPassable?: boolean
    colour?: string
    bgColour?: string
    content?: string
}
export default class GameCell {
    private passable: boolean
    private visible: boolean
    private content: string
    private colour: string
    private bgColour: string
    private viewers: Set<Entity>
    constructor(content: string, options: GameCellOptions = {}) {
        this.content = content;
        this.passable = options.isPassable || false;
        this.colour = options.colour || 'dimGrey';
        this.bgColour = options.bgColour || 'rosybrown';
        this.visible = false;
        this.viewers = new Set();
    }
    isPassable() { return this.passable }
    getVisibility() { return this.viewers.size }
    addViewer(viewer: Entity) { this.viewers.add(viewer)}
    removeViewer(viewer: Entity) { this.viewers.delete(viewer)}
    getContent() { return this.content }
    getColour() { return this.colour }
    getBgColour() { return this.bgColour }
    changeCell(options: GameCellOptions = {}) {
        this.content = options.content || this.content;
        this.colour = options.colour || this.colour;
        this.bgColour = options.bgColour || this.bgColour;
        this.passable = options.isPassable === false ? false : options.isPassable || this.passable;
    }
}
