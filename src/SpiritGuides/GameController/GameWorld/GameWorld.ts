import { Display, FOV } from "rot-js";
import GameMap from './GameMap/GameMap';
import { Coordinate, Entity } from '../../types';
import GameEngine from "../GameEngine/GameEngine";
import PreciseShadowcasting from "rot-js/lib/fov/precise-shadowcasting";
type Entities = {
    [key: string]: Entity
}


export default class GameWorld {
    private gameMap: GameMap
    private width: number
    private height: number
    private engine: GameEngine
    private display: Display
    private entities: Entities
    private fov: PreciseShadowcasting
    constructor(display: Display, engine: GameEngine) {
        this.display = display;
        this.width = display.getOptions().width;
        this.height = display.getOptions().height;
        this.engine = engine;
        this.gameMap = new GameMap(this.width, this.height);
        this.entities = {};
        this.fov = new FOV.PreciseShadowcasting((x,y) => {
            return this.isSeeThrough({x, y});
        });

        this.engine.addActor({
            act: async () => {
                this.forceScrollLeft()
            }
        }, true);

    }
    moveEntity(start: Coordinate, end: Coordinate) {
        if(!this.isPassable(end)) {
            console.log('cannot pass');
            return false;
        }
        const entity = this.getEntity(start);
        delete this.entities[`${start.x},${start.y}`];
        this.entities[`${end.x},${end.y}`] = entity;
        entity.setLocation(end);
        if(entity.isPlayer()) this.updateFieldOfVision(end, entity);

        this.draw();
        return true;
    }
    removeEntity(coordinate: Coordinate) {
        const entity = this.entities[`${coordinate.x},${coordinate.y}`];
        if(entity) {
            delete this.entities[`${coordinate.x},${coordinate.y}`];
            this.engine.removeActor(entity);
            entity.setLocation({x: -1, y: -1});
            this.removeViewer(entity);
            this.draw();
        }
    }
    addEntity(entity: Entity, coordinate: Coordinate) {
        if(this.isPassable(coordinate)) {
            this.entities[`${coordinate.x},${coordinate.y}`] = entity;
        } else {
            throw new Error('Cannot add to an unpassable space')
        }
        entity.setLocation(coordinate);
        this.engine.addActor(entity, true);
        if(entity.isPlayer()) this.updateFieldOfVision(coordinate, entity);
        this.draw();
    }
    addEntityForced(entity: Entity, coordinate: Coordinate) {
        if(!this.isPassable(coordinate)) {
            this.gameMap.clearCell(coordinate);
        }
        this.addEntity(entity, coordinate);
    }
    private getEntity(coordinate: Coordinate) {
        return this.entities[`${coordinate.x},${coordinate.y}`]
    }
    private isPassable(coordinate: Coordinate) {
        const content = this.getCellContent(coordinate);
        return Boolean(content.map && content.map.isPassable() && !content.entity);
    }
    private isSeeThrough(coordinate: Coordinate) {
        const content = this.getCellContent(coordinate);
        return Boolean(
            (content.map && content.map.isPassable()) &&
            (!content.entity || content.entity.isSeeThrough())
        );
    }
    getCellVisibility(coordinate: Coordinate) {
        const cell = this.gameMap.getCell(coordinate);
        return cell ? cell.getVisibility() : 0;
    }
    getCellContent(coordinate: Coordinate) {
        return {
            map: this.gameMap.getCell(coordinate),
            entity: this.getEntity(coordinate)
        };
    }
    draw() {
        this.gameMap.getMap().forEach((row, x) => {
            row.forEach((cell, y) => {
                const visibility = cell.getVisibility();
                if(visibility === 0) {
                    this.display.draw(x,y, '', 'darkgrey', 'darkgrey');
                } else {
                    const entity = this.getEntity({x, y});
                    const bgColour = visibility === 1 ? 'lightgrey' : cell.getBgColour();
                    if(entity) {
                        this.display.draw(x, y, entity.getAvatar(), cell.getColour(), bgColour);
                    } else {
                        this.display.draw(x, y, cell.getContent(), cell.getColour(), bgColour);
                    }
                }
            });
          });
    }
    getGameMap() { return this.gameMap }
    private removeViewer(viewer: Entity) {
        this.gameMap.getMap().forEach((row, x) => {
            row.forEach((cell, y) => {
                cell.removeViewer(viewer);
            });
        });
    }
    updateFieldOfVision(location: Coordinate, viewer: Entity) {
        this.removeViewer(viewer);
        const VIEWER_RANGE = 10;

        this.fov.compute(location.x, location.y, VIEWER_RANGE, (x: number, y: number, range: number) => {
            this.gameMap.getCell({x,y})?.addViewer(viewer);
        })
    }
    forceScrollLeft() {
        this.gameMap.exploreLeft();
        const entities = Object.values(this.entities);
        const newEntities: Entities = {}
        entities.forEach(entity => {
            const currentLocation = entity.getLocation();
            if(currentLocation.x < 2) {
                entity.lose();
                this.removeEntity(currentLocation);
                this.removeViewer(entity);
            } else {
                entity.setLocation({x: currentLocation.x-1, y: currentLocation.y});
                newEntities[`${currentLocation.x-1},${currentLocation.y}`] = entity;
            }
        });
        this.entities = newEntities;
        this.gameMap.destroyColumn(1);
        this.draw();
    }
};
