import GameWorld from './GameWorld';
import GameEngine from '../GameEngine/GameEngine';
import GameMap from './GameMap/GameMap';
import ROT from 'rot-js';
import Guide from '../../Team/Guide/Guide';
import GameCell from './GameMap/GameCell/GameCell';
import { Coordinate } from '../../types';
jest.mock('rot-js', () => {
    return {
        Display: jest.fn(),
        FOV: { PreciseShadowcasting: jest.fn()}
    }
});
jest.mock('./GameMap/GameMap', ()=> jest.fn());
jest.mock('../GameEngine/GameEngine', ()=> jest.fn());

const rotDisplayMock = ROT.Display as any;
const gameEngineConstructorMock = GameEngine as any;
const gameMapMock = GameMap as any;
const rotFovMock = ROT.FOV.PreciseShadowcasting as any;


describe('GameWorld', () => {
    const mockDisplayOptions = { width: 15, height: 15};
    const instantiatedEngineMock = { 
        addActor: jest.fn() ,
        removeActor: jest.fn()
    };
    const instantiatedDisplayMock = { 
        getOptions: () => (mockDisplayOptions),
        draw: jest.fn()
    };
    const instantiatedGameMapMock = {
        getCell: jest.fn(),
        clearCell: jest.fn(),
        getMap: jest.fn()
    };
    const instantiatedFovMock = {
        compute: jest.fn()
    }
    beforeEach(() => {
        rotDisplayMock.mockReturnValue(instantiatedDisplayMock);
        rotFovMock.mockReturnValue(instantiatedFovMock);
        gameEngineConstructorMock.mockReturnValue(instantiatedEngineMock)
        gameMapMock.mockReturnValue(instantiatedGameMapMock);
    });

    afterEach(() => {
        instantiatedDisplayMock.draw.mockReset();
        instantiatedGameMapMock.getCell.mockReset();
        instantiatedGameMapMock.clearCell.mockReset();
        instantiatedFovMock.compute.mockReset();
    });

    describe('instantiation', () => {
        test('instantiates a map of specified dimensions', () => {
            const gameWorld = new GameWorld(rotDisplayMock(), gameEngineConstructorMock());
            
            expect(gameMapMock).toHaveBeenCalledWith(mockDisplayOptions.width, mockDisplayOptions.height);
            expect(gameWorld.getGameMap()).toBe(instantiatedGameMapMock);
        });
    });

    describe('entity management', () => {
        let gameWorld: GameWorld;
        beforeEach(() => {
            const mockMap = [
                [new GameCell('Z'), new GameCell('Z'), new GameCell('Z')],
                [new GameCell('Z'), new GameCell('A', { isPassable: true}), new GameCell('A')],
                [new GameCell('Z'), new GameCell('A', { isPassable: true}), new GameCell('A')]
            ];
            instantiatedGameMapMock.getMap.mockReturnValue(mockMap);
            instantiatedGameMapMock.getCell.mockImplementation((coordinate: Coordinate) => mockMap[coordinate.x][coordinate.y]);
            gameWorld = new GameWorld(rotDisplayMock(), gameEngineConstructorMock());
            gameWorld.draw = jest.fn();
        });
        test('addEntity adds specified entity to specified location and adds them to the scheduler', () => {
            const guide = new Guide('1', 'A');
            const coordinate = {x:1, y:1};

            gameWorld.addEntity(guide, coordinate)

            expect(gameWorld.getCellContent(coordinate).entity).toBe(guide);
            expect(instantiatedEngineMock.addActor).toHaveBeenCalledWith(guide, true);
            expect(guide.getLocation()).toBe(coordinate);
            expect(gameWorld.draw).toHaveBeenCalled();
        });
        test('addEntity errors if specified location is not passable', () => {
            const guide = new Guide('1', 'A', jest.fn());
            const coordinate = {x:0, y:0};

            try {
                gameWorld.addEntity(guide, coordinate)
                expect('error thrown').toBe(true);
            } catch(error) {
                expect(error).toEqual(new Error('Cannot add to an unpassable space'));
            }

        });
        test('addEntityForced adds specified entity to specified location and removes unpassable terrain to permit it', () => {
            const gameWorld = new GameWorld(rotDisplayMock(), gameEngineConstructorMock());
            instantiatedGameMapMock.clearCell.mockImplementationOnce((coordinate: Coordinate) => instantiatedGameMapMock.getMap()[coordinate.x][coordinate.y] = new GameCell('A', {isPassable: true}))
            const guide = new Guide('1', 'A');
            const coordinate = {x:0, y:0};
            gameWorld.draw = jest.fn();

            gameWorld.addEntityForced(guide, coordinate)

            expect(gameWorld.getCellContent(coordinate).entity).toBe(guide);
            expect(instantiatedGameMapMock.clearCell).toHaveBeenCalledWith(coordinate);
            expect(gameWorld.draw).toHaveBeenCalled();
        });
        test.only('draw will draw the entity avatar if one is present', () => {
            const gameWorld = new GameWorld(rotDisplayMock(), gameEngineConstructorMock());
            const guide = new Guide('1', 'Z');
            const coordinate = {x:1, y:1};

            const guideCell = instantiatedGameMapMock.getMap()[1][1].getVisibility = jest.fn(()=>3);
            gameWorld.addEntity(guide, coordinate)
            expect(instantiatedDisplayMock.draw).toHaveBeenCalledTimes(9);
            expect(instantiatedDisplayMock.draw).toHaveBeenNthCalledWith(5, 1,1, 'Z', "dimGrey", "rosybrown");
        });
        test('moveEntity will move the entity if the destination is passable', () => {
            const guide = new Guide('1', 'Z');
            const coordinate = {x:1, y:1};
            const destination = { x: 2, y: 1};

            gameWorld.addEntity(guide, coordinate)
            gameWorld.getCellContent(coordinate)
            const isMoved = gameWorld.moveEntity(coordinate, destination);
            expect(isMoved).toBe(true);
            expect(gameWorld.getCellContent(coordinate).entity).toBe(undefined);
            expect(gameWorld.getCellContent(destination).entity).toBe(guide);
            expect(guide.getLocation()).toBe(destination);
        });
        test('moveEntity will not move the entity if the destination is not passable', () => {
            const guide = new Guide('1', 'Z');
            const coordinate = {x:1, y:1};
            const destination = { x: 0, y: 0};
            gameWorld.addEntity(guide, coordinate);
            const isMoved = gameWorld.moveEntity(coordinate, destination);
            expect(isMoved).toBe(false);
            expect(gameWorld.getCellContent(coordinate).entity).toBe(guide);
            expect(gameWorld.getCellContent(destination).entity).toBe(undefined);
            expect(guide.getLocation()).toBe(coordinate);
        });
        test('removeEntity removes the entity at a given location', () => {
            const guide = new Guide('1', 'Z');
            const coordinate = {x:1, y:1};

            gameWorld.addEntity(guide, coordinate);
            gameWorld.removeEntity(coordinate);
            expect(gameWorld.getCellContent(coordinate).entity).toBe(undefined);
            expect(instantiatedEngineMock.removeActor).toHaveBeenCalledWith(guide);
        });
    });

    describe('visibility management', () => {
        test('getCellVisibility returns the visibilty of given coordinate', () => {

        });
    });
});
