import { Display } from 'rot-js';
import initialiseGameDisplay from './GameDisplay/initialiseGameDisplay'
import { DisplayOptions } from 'rot-js/lib/display/types';
import GameWorld from './GameWorld/GameWorld';
import GameEngine from './GameEngine/GameEngine';
import Team from '../Team/Team';
import PlayerController from './PlayerController/PlayerController';


const defaultDisplayOptions: Partial<DisplayOptions> = {
    width: 15,
    height: 15,
    fontFamily: 'Fira Mono',
    fontSize: 30, // canvas fontsize
    forceSquareRatio: true // make the canvas squared ratio
};

export enum GameState {
    Pregame = 0,
    Started,
    Loss,
    Win 
}
const noop = () => {}
export default class GameController {
    private display: Display
    private engine: GameEngine
    private gameState: GameState = 0
    private gameWorld: GameWorld
    private winCallback: Function
    private loseCallback: Function
    private playerController: PlayerController
    constructor(team: Team, winCallback = noop, loseCallback = noop) {
        const displayOptions = defaultDisplayOptions;
        this.display = initialiseGameDisplay(displayOptions);
        this.gameState = GameState.Started;
        this.winCallback = winCallback;
        this.loseCallback = loseCallback;
        this.engine = new GameEngine();
        this.gameWorld = new GameWorld(this.display, this.engine);
        this.playerController = new PlayerController(team, this.gameWorld, this.engine, this.triggerWin.bind(this), this.triggerLose.bind(this));
        this.engine.start();
    }
    triggerWin() {
        this.gameState = GameState.Win;
        this.winCallback();
    }
    triggerLose() {
        if(this.gameState !== GameState.Loss) {
            this.gameState = GameState.Loss
            this.loseCallback();
        }
    }
    getGameState() { return this.gameState;}
}
