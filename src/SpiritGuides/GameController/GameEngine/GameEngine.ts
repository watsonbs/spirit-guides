import { Engine, Scheduler } from "rot-js"
import Simple from "rot-js/lib/scheduler/simple"
import { Actor } from "../../types"

export default class GameEngine {
    private engine: Engine
    private scheduler: Simple
    constructor() {
        this.scheduler = new Scheduler.Simple();
        this.engine = new Engine(this.scheduler);
    }
    addActor(actor: Actor, isRepeated: boolean) { this.scheduler.add(actor, isRepeated); }
    removeActor(actor: Actor) { this.scheduler.remove(actor)}
    start() { this.engine.start()}
    lock() { this.engine.lock()}
    unlock() { this.engine.unlock()}
}
