import { DIRS } from "rot-js";
import Team from "../../Team/Team";
import GameEngine from "../GameEngine/GameEngine";
import GameWorld from "../GameWorld/GameWorld";

type KeyMap = {
    [key: string]: number 
}

export default class PlayerController {
    private team: Team
    private gameWorld: GameWorld;
    private gameEngine: GameEngine;
    private winCallback: Function;
    private loseCallback: Function;
    constructor(team: Team, gameWorld: GameWorld, gameEngine: GameEngine, winCallback = () => {}, loseCallback = () => {}) {
        this.team = team;
        this.gameWorld = gameWorld;
        this.gameEngine = gameEngine;
        this.winCallback = winCallback;
        this.loseCallback = loseCallback;
        this.addTeamToWorld();
    }
    private checkTeamVisibility() {
        const teamMembers = this.team.getMembers();
        if(teamMembers.length === 0) {
            this.loseCallback(); 
            return;
        }
        let memberLost = false;
        teamMembers.forEach(teamMember => {
            if(memberLost) return;
            const location = teamMember.getLocation();
            const isSeen = this.gameWorld.getCellVisibility(location);
            if(isSeen < 2) {
                this.gameWorld.removeEntity(location);
                teamMember.lose();
                memberLost = true;
            }
        });
        if(memberLost) this.checkTeamVisibility();
    }
    private addTeamToWorld() {
        const maxStartingX = 2;
        const minStartingX = 2;
        const minStartingY = 1;
        this.team.getMembers().forEach((guide, i) => {
            const outOfBounds = maxStartingX - minStartingX + 1;
            const x = (i % outOfBounds) + minStartingX;
            const y = Math.floor(i / outOfBounds)+minStartingY;
            this.gameWorld.addEntityForced(guide, {x, y});

            guide.setAction(async () => {
                if(guide.isLost()) {
                    console.error('lost guide activated');
                    console.log(guide);
                    return;
                }
                this.gameEngine.lock();
                guide.activate();
                const action = (event: KeyboardEvent)=> {
                    
                    const keyMap: KeyMap = {
                        KeyW: 0, //w
                        ArrowUp: 0,
                        33: 1, //[1,-1]
                        KeyD: 2, //d
                        ArrowRight: 2,
                        34: 3, //[1,1]
                        KeyS: 4, //s
                        ArrowDown: 4,
                        35: 5, //[-1,1]
                        KeyA: 6, //a
                        ArrowLeft: 6,
                        36: 7, //[-1,-1]
                    };
                    const code = event.code;
                    if(!(code in keyMap)) return;
                    
                    const diff = DIRS[8][keyMap[code]];
                    
                    if(guide.getAvailableActionPoints() > 0) {
                        const start = guide.getLocation();
                        const destination = { x: start.x + diff[0], y: start.y + diff[1]};
                        const isMoved = this.gameWorld.moveEntity(start, destination);
                        if(isMoved) {
                            guide.subtractActionPoints(1);
                            this.checkTeamVisibility();
                        }
                    }
                    if(guide.getAvailableActionPoints() < 1) {
                        this.gameEngine.unlock();
                        guide.deactivate();
                        window.removeEventListener("keydown",action);
                    }
                    
                };
                window.addEventListener("keydown",action);
            })
        });

        
    }
}
