import Team from '../../Team/Team';
import GameEngine from '../GameEngine/GameEngine';
import GameWorld from '../GameWorld/GameWorld';
import PlayerController from './PlayerController';
jest.mock('../GameWorld/GameWorld', ()=> jest.fn());

jest.mock('../GameEngine/GameEngine', ()=> jest.fn());

const mockGameWorld = GameWorld as any;
const mockGameEngine = GameEngine as any;

describe('PlayerController', () => {
    test('adds the team to gameWorld', () => {
        const mockTeam = new Team(3);
        const teamMembers = mockTeam.getMembers();
        const mockInstantiatedGameWorld = { 
            addEntityForced: jest.fn(),
            getCellVisibility: jest.fn()
        };        
        const mockInstantiatedGameEngine = { 
            start: jest.fn()
        };
        mockGameWorld.mockReturnValueOnce(mockInstantiatedGameWorld);
        mockGameWorld.mockReturnValueOnce(mockInstantiatedGameEngine);
        const playerController = new PlayerController(mockTeam, mockGameWorld(), mockGameEngine());
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledTimes(3)
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[0], {x:1, y:1});
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[1], {x:1, y:2});
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[2], {x:1, y:3});
    });

    test('adds the team to gameWorld for other numbers of team members', () => {
        const mockTeam = new Team(5);
        const teamMembers = mockTeam.getMembers();
        const mockInstantiatedGameWorld = { 
            addEntityForced: jest.fn(),
            getCellVisibility: jest.fn()
        };
        mockGameWorld.mockReturnValueOnce(mockInstantiatedGameWorld);
        const playerController = new PlayerController(mockTeam, mockGameWorld(), mockGameEngine());
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledTimes(5)
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[0], {x:1, y:1});
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[1], {x:1, y:2});
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[2], {x:1, y:3});
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[3], {x:1, y:4});
        expect(mockInstantiatedGameWorld.addEntityForced).toHaveBeenCalledWith(teamMembers[4], {x:1, y:5});
    });

    describe('player actions', () => {
        let realEventListener = window.addEventListener;
        let events = {} as any;
        beforeEach(()=> {
            realEventListener = window.addEventListener;
            events = {};
            window.addEventListener = jest.fn((event, cb) => {
                events[event] = cb;
              });
        });
        afterAll(() => {
            window.addEventListener = realEventListener;
        });
        test('adds the teamAction as the action to each team member', () => {
            const mockTeam = new Team(3);
            const teamMembers = mockTeam.getMembers();
            const mockInstantiatedGameWorld = { 
                addEntityForced: jest.fn(),
                moveEntity: jest.fn(()=> true),
                getCellVisibility: jest.fn()
            };        
            const mockInstantiatedGameEngine = { 
                start: jest.fn(),
                lock: jest.fn(),
                unlock: jest.fn()
            };
            mockGameWorld.mockReturnValueOnce(mockInstantiatedGameWorld);
            mockGameEngine.mockReturnValueOnce(mockInstantiatedGameEngine);
            const playerController = new PlayerController(mockTeam, mockGameWorld(), mockGameEngine());
            teamMembers[0].setLocation({x: 1, y: 1});
            teamMembers[0].act();
            expect(mockInstantiatedGameEngine.lock).toHaveBeenCalled();
            expect(teamMembers[0].isActive()).toBe(true);
            events.keydown({code: 'KeyA'});
            expect(mockInstantiatedGameWorld.moveEntity).toHaveBeenCalledWith({"x": 1, "y": 1}, {"x": 0, "y": 1});
            mockInstantiatedGameWorld.moveEntity.mockClear();
            events.keydown({code: 'KeyW'});
            expect(mockInstantiatedGameWorld.moveEntity).toHaveBeenCalledWith({"x": 1, "y": 1}, {"x": 1, "y": 0});
            mockInstantiatedGameWorld.moveEntity.mockClear();
            events.keydown({code: 'KeyD'});
            expect(mockInstantiatedGameWorld.moveEntity).toHaveBeenCalledWith({"x": 1, "y": 1}, {"x": 2, "y": 1});
            expect(mockInstantiatedGameEngine.unlock).toHaveBeenCalled();
            expect(teamMembers[0].isActive()).toBe(false);
        });
    })
    
});
