import GameController, {GameState} from './GameController';
import initialiseGameDisplay from './GameDisplay/initialiseGameDisplay';
import GameWorld from './GameWorld/GameWorld';
import GameEngine from './GameEngine/GameEngine';
import PlayerController from './PlayerController/PlayerController';
import Team from '../Team/Team';

jest.mock('./GameDisplay/initialiseGameDisplay', () => jest.fn());
jest.mock('./GameWorld/GameWorld', () => jest.fn());
jest.mock('./GameEngine/GameEngine');
jest.mock('./PlayerController/PlayerController', () => jest.fn());


const initialiseGameDisplayMock = initialiseGameDisplay as any;
const mockTeam = new Team(1);
const gameWorldMock = GameWorld as any;
const gameEngineMock = GameEngine as any;

describe('GameController', () => {
    const mockInstatiatedGameEngine = { start: jest.fn() };
    beforeEach(() => {
        gameEngineMock.mockReturnValue(mockInstatiatedGameEngine);
    });
    test('initialises the PlayerController and its dependencies', () => {
        const mockDisplay = { myDisplay: true};
        const mockGameWorld = { myGameWorld: true};
        initialiseGameDisplayMock.mockReturnValueOnce(mockDisplay);
        gameWorldMock.mockReturnValueOnce(mockGameWorld);
        
        const controller = new GameController(mockTeam);
        
        expect(initialiseGameDisplay).toHaveBeenCalled();
        expect(gameEngineMock).toHaveBeenCalled();
        expect(GameWorld).toHaveBeenCalledWith(mockDisplay, mockInstatiatedGameEngine);
        expect(PlayerController).toHaveBeenCalledWith(mockTeam, mockGameWorld, mockInstatiatedGameEngine, expect.any(Function), expect.any(Function));
    });

    describe('game state', () => {
        const loseCallback = jest.fn();
        const winCallback = jest.fn();
        const controller = new GameController(mockTeam, winCallback, loseCallback)
        test('starting sets the game state to Started', () => {
            expect(controller.getGameState()).toBe(GameState.Started);
        });
        test('winning sets the game state to Win and triggers the callback', () => {
            controller.triggerWin();
            expect(controller.getGameState()).toBe(GameState.Win);
            expect(winCallback).toHaveBeenCalled();
        });
        test('losing sets the game state to Lose and triggers the loseCallback', () => {
            controller.triggerLose();
            expect(controller.getGameState()).toBe(GameState.Loss);
            expect(loseCallback).toHaveBeenCalled();
        });
    })
});
