import { render } from '@testing-library/react';
import SpiritGuides from '.';
import GameController from './GameController/GameController';

jest.mock('./GameController/GameController', () => jest.fn());


describe('SpiritGuides index', () => {
    test('starts a new game', () => {
        render(<SpiritGuides></SpiritGuides>);
        expect(GameController).toHaveBeenCalled();
    });
});
