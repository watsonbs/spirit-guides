import React from 'react';
import Team from '../Team/Team';
import './teamDisplay.scss';

interface IProps {
    team: Team
}

export default class TeamDisplay extends React.Component<IProps> {
    componentDidMount() {
        this.props.team.addWatcher(() => {
            this.forceUpdate()
        });
    }
    render() {
        return (
            <div>
                <ul className="sg-teamMember-list">
                    {
                        this.props.team.getMembers().map(guide => (
                            <li key={guide.getId()} className={
                                guide.isActive() ? "sg-teamMember sg-teamMember--active": "sg-teamMember"
                            } >
                                <span>{guide.getAvatar()}</span>
                                <span>{guide.getName()}</span>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    }
}
