import { render } from '@testing-library/react';
import Team from '../Team/Team';
import TeamDisplay from './TeamDisplay';

const mockTeam = new Team(3);

describe('TeamDisplay', () => {
    test('displays the provided team', () => {
        const props = {team: mockTeam};
        render(<TeamDisplay {...props}></TeamDisplay>);
        expect(document.getElementsByTagName('li').length).toBe(3);
    });
});
