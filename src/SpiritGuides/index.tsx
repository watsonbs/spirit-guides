import React from 'react';
import GameController from './GameController/GameController';
import Team from './Team/Team';
import TeamDisplay from './TeamDisplay/TeamDisplay';

interface IProps {

}
interface IState {
    team: Team,
    game: GameController
}

export default class SpiritGuides extends React.Component<IProps, IState> {
    winGame(){
        alert("You win");
    }
    loseGame() {
        alert("You lose");
    }
    componentDidMount() {
        this.startGame();
    }
    startGame() {
        const team = new Team(3);
        const game = new GameController(team, this.winGame, this.loseGame);
        this.setState({game, team});
    }
    render() {
        return (
            <div>
                <div>
                    <button onClick={()=>this.startGame()}>Restart</button>
                </div>
                <div id="canvas"></div>
                { this.state && this.state.team ? 
                    <TeamDisplay {...{team: this.state.team}}></TeamDisplay>
                : ''}
            </div>
        )
    }
}
